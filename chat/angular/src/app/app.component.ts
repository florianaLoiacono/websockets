import { Component } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private router: Router) {
    if (this.router.url == '/')
    {
      this.buttonOption = "chat";
    }
    else
    {
      this.buttonOption = "home";
    }
  }

  public buttonOption: string = "";

  public toDoList: Array<object> = [{
    id: 1,
    name: 'Agregar nombre del usuario y redirigir al chat',

  }, {
    id: 2,
    name: 'Observers angular'
  }];

}
