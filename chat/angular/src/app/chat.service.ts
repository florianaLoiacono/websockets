import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

const URL: string = 'ws://localhost:8080';

export enum typeMessageEnum {
  ERROR = -1,
  CONNECTION = 0,
  GREETINGS,
  USER_MESSAGE,
  SYSTEM_MESSAGE
};

export interface WSMessage {
  id: number,
  typeMessage: typeMessageEnum,
  from: number,
  to: number,
  data: object
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public username: string = '';
  private ws: any;
  public id = new BehaviorSubject(0);
  public wsObservable: Observable<any>;

  public users_connected = new BehaviorSubject(new Array<string>());
  constructor() {
  }

  public isAlreadyIn(): boolean {
    if (this.ws != null) return true;
    return false;
  }

  public connectToWS(username: string): Promise<any> {
    this.username = username;
    return new Promise((resolve, reject) => {
      if (this.ws != null) return reject('Session already created');
      this.ws = new WebSocket(URL);
      this.ws.onopen = () => {
        this.ws.send(JSON.stringify({
          typeMessage: typeMessageEnum.CONNECTION,
          message: 'Hello server!',
          data: { username: username }
        }));
      }

      this.wsObservable = new Observable((subscriber) => {
        this.ws.onmessage = (msg) => {
          // console.log(JSON.parse(msg.data), 'Observer onMessage');
          let result = this.msgProcess(JSON.parse(msg.data));
          if (!result) { return; }
          subscriber.next(result);
        }
      })
      return resolve(true);
    })
  }


  public sendToAll(message: string) {
    this.ws.send(JSON.stringify({
      typeMessage: typeMessageEnum.USER_MESSAGE,
      from: this.id.getValue(),
      data: { message }
    }));
  }

  public sendToUser(to: string, message: string) {
    this.ws.send(JSON.stringify({
      typeMessage: typeMessageEnum.USER_MESSAGE,
      from: this.id.getValue().toString(),
      to: to,
      data: { message }
    }));
  }

  private msgProcess(msg: WSMessage) {
    switch (msg.typeMessage) {
      case typeMessageEnum.USER_MESSAGE:
        return {
          from: msg.from.toString(),
          message: msg.data['message'],
          time: msg.data['time']
        };
        break;
      case typeMessageEnum.SYSTEM_MESSAGE:
        if (msg.data['users_connected']) {
          this.users_connected.next(msg.data['users_connected']);
        }
        break;
      case typeMessageEnum.GREETINGS:
        console.log('Greetings')
        this.id.next(msg.data['id']);

        if (msg.data['users_connected']) {
          this.users_connected.next(msg.data['users_connected']);
        }
        break; case typeMessageEnum.ERROR:
        break;

    }
  }




}
