import { Component, OnInit } from '@angular/core';
import { ChatService } from '../chat.service';

const URL: string = 'ws://localhost:8080';

interface Chat {
  from: string,
  message: string,
  time: Date
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  ngOnInit(): void {
  }

  public objectsLog: string = "";
  public username!: string;
  public users_connected: object;
  public usersIDs: Array<string>;
  public id: number = 0;
  public user_selected: string;
  public chatArr: Array<Chat> = new Array<Chat>();

  constructor(private chatService: ChatService) {
    this.username = chatService.username;
    console.log(chatService.username, 'chatService.username')

    this.chatService.id.subscribe({ next: value => { this.id = value } });
    this.chatService.users_connected.subscribe({
      next: value => {
        this.usersIDs = Object.keys(value);
        this.users_connected = value
      }
    });
    this.chatService.wsObservable.subscribe({
      next: value => {
        this.chatArr.push({
          from: this.users_connected[value.from],
          message: value.message,
          time: new Date(value.time)
        });
      },
      error(err) {

      }, complete() {

      },
    })
  }

  public send(message: any) {
    this.chatService.sendToAll(message);
  }

  public sendToUser(message: any) {
    this.chatService.sendToUser(this.user_selected, message);  // this.wsService.send(message);
  }
}
