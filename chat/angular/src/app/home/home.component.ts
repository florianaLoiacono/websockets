import { Component, Input, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public username = new FormControl('', [Validators.required]);

  private usernameObserver: Observable<FormControl>;

  constructor(private chatService: ChatService, private router: Router) {
    if (chatService.isAlreadyIn())
      this.router.navigateByUrl('/chat')
  }

  ngOnInit(): void {
  }

  public letsChat(username: string) {
    if (!this.username.valid) return;
    this.chatService.connectToWS(username).then(x =>
      this.router.navigateByUrl('/chat')
    ).catch(x => {
      console.error(x);
      if (x == 'Session already created')
        this.router.navigateByUrl('/chat')

    });

  }
}
