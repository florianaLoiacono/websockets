const express = require('express');
const WebSocket = require('ws');

const app = express();


app.use(express.static('./angular'));
app.get('/', (req, res, next) => {
    res.render('index.html');
});

const ws = new WebSocket.Server({
    noServer: true
});

const msgTypeEnum = {
    ERROR: -1,
    CONNECTION: 0,
    GREETINGS: 1,
    USER_MESSAGE: 2,
    SYSTEM_MESSAGE: 3
};


ws.on('connection', function connection(ws) {
    ws.on('message', function message(data) {
        console.log('received: %s', data);
    });

    ws.send('something');
});

let socketsDic = {};
let usernamesDic = {};

ws.on('connection', (socket) => {
    let id = (new Date()).getTime();
    socketsDic[id] = socket;
    console.log('connection');

    socket.on('message', (msg) => {

        let obj = JSON.parse(msg.toString());
        console.log(msg.toString(), 'on message');
        if (typeof (obj) == 'object' && obj['typeMessage'] !== undefined) {

            switch (obj.typeMessage) {
                case msgTypeEnum.CONNECTION:
                    if (!obj.data.username) {
                        sendError('usename not specified', id);
                        break;
                    }
                    console.log(`user conected ${id}`);
                    usernamesDic[id] = obj.data.username;
                    sendUserGreetings(id);
                    refreshUserConnectedList(id);
                    break;
                case msgTypeEnum.USER_MESSAGE:
                    if (obj.to) {
                        console.log(obj.to, 'send to');
                        socketsDic[obj.to].send(
                            JSON.stringify({
                                typeMessage: msgTypeEnum.USER_MESSAGE,
                                data: {
                                    message: obj.data.message,
                                    time: (new Date()).getTime()
                                },
                                from: obj.from,
                            }));
                    } else {
                        Object.keys(socketsDic).forEach(x => {
                            socketsDic[x].send(
                                JSON.stringify({
                                    typeMessage: msgTypeEnum.USER_MESSAGE,
                                    data: {
                                        message: obj.data.message,
                                        time: (new Date()).getTime()
                                    },
                                    from: obj.from,
                                }));
                        })
                    }
                    break;
            }
        }
    });

    socket.on('close', () => {
        delete socketsDic[id];
        delete usernamesDic[id];
        refreshUserConnectedList(id);
        console.log(id, 'id removed');
    })
});

const refreshUserConnectedList = () => {

    Object.values(socketsDic).forEach(x => {
        x.send(JSON.stringify({
            typeMessage: msgTypeEnum.SYSTEM_MESSAGE,
            data: {
                users_connected: usernamesDic
            },
        }));
    })
    return;
}

const sendUserGreetings = (id) => {
    socketsDic[id].send(JSON.stringify({
        typeMessage: msgTypeEnum.GREETINGS,
        data: {
            id
        }
    }));
};

const endError = function (message, id = null) {
    if (id == null) {
        socketsDic.forEach(x => {
            x.send(JSON.stringify({
                typeMessage: msgTypeEnum.ERROR,
                message
            }))
        });
        return;
    }
    socketsDic[id].send(JSON.stringify({
        typeMessage: msgTypeEnum.ERROR,
        message
    }));
    return;
};

console.log("Hi sockets");

const server = app.listen(8080, () => {
    console.log('Example app listening on port')
});

server.on('upgrade', (request, socket, head) => {
    ws.handleUpgrade(request, socket, head, socket => {
        ws.emit('connection', socket, request);
    });
});